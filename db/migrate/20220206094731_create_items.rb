class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items, id: false do |t|
      t.string :item_number, primary_key: true
      t.string :item_name
      t.integer :amount

      t.timestamps
    end
  end
end
