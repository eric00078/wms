class CreateInwardOrderDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :inward_order_details do |t|
      t.string :order_number
      t.string :serial_number
      t.string :item_number
      t.integer :amount
      t.string :status

      t.timestamps
    end
  end
end
