class CreateStocks < ActiveRecord::Migration[7.0]
  def change
    create_table :stocks, id: false do |t|
      t.string :storage_id, primary_key: true
      t.string :item_number
      t.integer :amount

      t.timestamps
    end
  end
end
