class CreateInwardOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :inward_orders, id: false do |t|
      t.string :order_number, primary_key: true
      t.string :status

      t.timestamps
    end
  end
end
