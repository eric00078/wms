class CreateStorages < ActiveRecord::Migration[7.0]
  def change
    create_table :storages, id: false  do |t|
      t.string :storage_id, primary_key: true
      t.boolean :in_use

      t.timestamps
    end
  end
end
