# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
#
#Item.create(item_number: "item1", item_name: "item1", amount: 500)
#Item.create(item_number: "item2", item_name: "item2", amount: 500)
#Storage.create(storage_id: "storage1", in_use: 1)
#Storage.create(storage_id: "storage2", in_use: 1)
#Storage.create(storage_id: "storage3", in_use: 1)
#Storage.create(storage_id: "storage4", in_use: 1)
#Storage.create(storage_id: "storage5", in_use: 0)
#Storage.create(storage_id: "storage6", in_use: 0)
#Storage.create(storage_id: "storage7", in_use: 0)
#Storage.create(storage_id: "storage8", in_use: 0)
#Storage.create(storage_id: "storage9", in_use: 0)
#Storage.create(storage_id: "storage10", in_use: 0)
#Stock.create(storage_id: "storage1", item_number: "item1", amount: 250)
#Stock.create(storage_id: "storage2", item_number: "item1", amount: 250)
#Stock.create(storage_id: "storage3", item_number: "item2", amount: 250)
#Stock.create(storage_id: "storage4", item_number: "item2", amount: 250)
#InwardOrder.create(order_number: "inward_order1", status: "initial")
#InwardOrderDetail.create(order_number: "inward_order1", serial_number: "1", item_number: "item1", amount: 300, status: "initial")
#InwardOrder.create(order_number: "inward_order2", status: "initial")
#InwardOrderDetail.create(order_number: "inward_order2", serial_number: "1", item_number: "item2", amount: 300, status: "initial")
#OutwardOrder.create(order_number: "outward_order1", status: "initial")
#OutwardOrderDetail.create(order_number: "outward_order1", serial_number: "1", item_number: "item1", amount: 300, status: "initial")
#OutwardOrder.create(order_number: "outward_order2", status: "initial")
#OutwardOrderDetail.create(order_number: "outward_order2", serial_number: "1", item_number: "item2", amount: 300, status: "initial")


