Rails.application.routes.draw do
  resources :inward_order_details do 
    post :inward, on: :member
  end
  resources :outward_order_details do 
    post :outward, on: :member
  end
  resources :outward_orders do
    get :orders_with_status, on: :collection
  end
  resources :inward_orders do
    get :orders_with_status, on: :collection
  end
  resources :storages do 
    get :empty_one, on: :collection
    get :empty_all, on: :collection
  end
  resources :stocks do
    post :item_stocks, on: :collection
  end
  resources :items
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  
  root 'items#index'
end
