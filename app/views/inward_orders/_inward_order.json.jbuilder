json.extract! inward_order, :id, :order_number, :status, :created_at, :updated_at
json.url inward_order_url(inward_order, format: :json)
