json.extract! inward_order_detail, :id, :order_number, :serial_number, :item_number, :amount, :status, :created_at, :updated_at
json.url inward_order_detail_url(inward_order_detail, format: :json)
