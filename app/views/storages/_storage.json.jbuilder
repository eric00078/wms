json.extract! storage, :id, :storage_id, :in_use, :created_at, :updated_at
json.url storage_url(storage, format: :json)
