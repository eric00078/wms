json.extract! stock, :id, :storage_id, :item_number, :amount, :created_at, :updated_at
json.url stock_url(stock, format: :json)
