json.extract! item, :id, :item_number, :item_name, :amount, :created_at, :updated_at
json.url item_url(item, format: :json)
