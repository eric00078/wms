json.extract! outward_order, :id, :order_number, :status, :created_at, :updated_at
json.url outward_order_url(outward_order, format: :json)
