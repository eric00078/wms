class OutwardOrderDetailsController < ApplicationController
  before_action :set_outward_order_detail, only: %i[ show edit update destroy outward ]
  protect_from_forgery with: :null_session

  # GET /outward_order_details or /outward_order_details.json
  def index
    @outward_order_details = OutwardOrderDetail.all
  end

  # GET /outward_order_details/1 or /outward_order_details/1.json
  def show
  end

  # GET /outward_order_details/new
  def new
    @outward_order_detail = OutwardOrderDetail.new
  end

  # GET /outward_order_details/1/edit
  def edit
  end

  # POST /outward_order_details or /outward_order_details.json
  def create
    @outward_order_detail = OutwardOrderDetail.new(outward_order_detail_params)

    respond_to do |format|
      if @outward_order_detail.save
        format.html { redirect_to outward_order_detail_url(@outward_order_detail), notice: "Outward order detail was successfully created." }
        format.json { render :show, status: :created, location: @outward_order_detail }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @outward_order_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /outward_order_details/1 or /outward_order_details/1.json
  def update
    respond_to do |format|
      if @outward_order_detail.update(outward_order_detail_params)
        format.html { redirect_to outward_order_detail_url(@outward_order_detail), notice: "Outward order detail was successfully updated." }
        format.json { render :show, status: :ok, location: @outward_order_detail }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @outward_order_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /outward_order_details/1 or /outward_order_details/1.json
  def destroy
    @outward_order_detail.destroy

    respond_to do |format|
      format.html { redirect_to outward_order_details_url, notice: "Outward order detail was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def outward
    ActiveRecord::Base.transaction do
      update_stock
      update_storage
      update_item
      update_outward_order_detail
      update_outward_order
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_outward_order_detail
      @outward_order_detail = OutwardOrderDetail.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def outward_order_detail_params
      params.require(:outward_order_detail).permit(:order_number, :serial_number, :item_number, :amount, :status)
    end

    def update_stock
      @stock = Stock.find(params[:storage_id])
      @stock.amount = @stock.amount-@outward_order_detail.amount
      if @stock.amount == 0
        @stock.delete
      else
        @stock.save
      end
    end

    def update_storage
      @storage = @stock.storage
      if @stock.amount == 0
        @storage.in_use = 0
        @storage.save
      end
    end

    def update_item
      @item = @stock.item
      @item.amount = @item.amount-@outward_order_detail.amount
      @item.save
    end

    def update_outward_order_detail
      @outward_order_detail.status = "done"
      @outward_order_detail.save
    end

    def update_outward_order
      @outward_order = @outward_order_detail.outward_order
      p @outward_order
      @outward_order_details = @outward_order.outward_order_details
      @outward_order_details.each do |current_order_detail|
        if current_order_detail.status.equal?("initial")
          return
        end
      end
      @outward_order.status = "done"
      @outward_order.save
    end

end
