class InwardOrderDetailsController < ApplicationController
  before_action :set_inward_order_detail, only: %i[ show edit update destroy inward ]
  protect_from_forgery with: :null_session

  # GET /inward_order_details or /inward_order_details.json
  def index
    @inward_order_details = InwardOrderDetail.all
  end

  # GET /inward_order_details/1 or /inward_order_details/1.json
  def show
  end

  # GET /inward_order_details/new
  def new
    @inward_order_detail = InwardOrderDetail.new
  end

  # GET /inward_order_details/1/edit
  def edit
  end

  # POST /inward_order_details or /inward_order_details.json
  def create
    @inward_order_detail = InwardOrderDetail.new(inward_order_detail_params)

    respond_to do |format|
      if @inward_order_detail.save
        format.html { redirect_to inward_order_detail_url(@inward_order_detail), notice: "Inward order detail was successfully created." }
        format.json { render :show, status: :created, location: @inward_order_detail }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @inward_order_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inward_order_details/1 or /inward_order_details/1.json
  def update
    respond_to do |format|
      if @inward_order_detail.update(inward_order_detail_params)
        format.html { redirect_to inward_order_detail_url(@inward_order_detail), notice: "Inward order detail was successfully updated." }
        format.json { render :show, status: :ok, location: @inward_order_detail }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @inward_order_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inward_order_details/1 or /inward_order_details/1.json
  def destroy
    @inward_order_detail.destroy

    respond_to do |format|
      format.html { redirect_to inward_order_details_url, notice: "Inward order detail was successfully destroyed." }
      format.json { head :no_content }
    end
  end
  
  def inward
    ActiveRecord::Base.transaction do
      update_stock
      update_storage
      update_item
      update_inward_order_detail
      update_inward_order
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inward_order_detail
      @inward_order_detail = InwardOrderDetail.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def inward_order_detail_params
      params.require(:inward_order_detail).permit(:order_number, :serial_number, :item_number, :amount, :status)
    end

    def update_stock
      @stock = Stock.new
      @stock.storage_id = params[:storage_id]
      @stock.item_number = @inward_order_detail.item_number
      @stock.amount = @inward_order_detail.amount
      @stock.save
    end

    def update_storage
      @storage = @stock.storage
      @storage.in_use = 1
      @storage.save
    end

    def update_item
      @item = @stock.item
      @item.amount = @item.amount+@stock.amount
      @item.save
    end

    def update_inward_order_details
      @inward_order_detail.status = "done"
      @inward_order_detail.save
    end

    def update_inward_order
      @inward_order = @inward_order_detail.inward_order
      p @inward_order
      @inward_order_details = @inward_order.inward_order_details
      @inward_order_details.each do |current_order_detail|
        if current_order_detail.status.equal?("initial")
          return
        end
      end
      @inward_order.status = "done"
      @inward_order.save
    end
end
