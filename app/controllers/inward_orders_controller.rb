class InwardOrdersController < ApplicationController
  before_action :set_inward_order, only: %i[ show edit update destroy ]

  # GET /inward_orders or /inward_orders.json
  def index
    @inward_orders = InwardOrder.all
  end

  # GET /inward_orders/1 or /inward_orders/1.json
  def show
  end

  # GET /inward_orders/new
  def new
    @inward_order = InwardOrder.new
  end

  # GET /inward_orders/1/edit
  def edit
  end

  # POST /inward_orders or /inward_orders.json
  def create
    @inward_order = InwardOrder.new(inward_order_params)

    respond_to do |format|
      if @inward_order.save
        format.html { redirect_to inward_order_url(@inward_order), notice: "Inward order was successfully created." }
        format.json { render :show, status: :created, location: @inward_order }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @inward_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inward_orders/1 or /inward_orders/1.json
  def update
    respond_to do |format|
      if @inward_order.update(inward_order_params)
        format.html { redirect_to inward_order_url(@inward_order), notice: "Inward order was successfully updated." }
        format.json { render :show, status: :ok, location: @inward_order }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @inward_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inward_orders/1 or /inward_orders/1.json
  def destroy
    @inward_order.destroy

    respond_to do |format|
      format.html { redirect_to inward_orders_url, notice: "Inward order was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def orders_with_status
    render json: InwardOrder.find_orders_by_status(params[:status])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inward_order
      @inward_order = InwardOrder.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def inward_order_params
      params.require(:inward_order).permit(:order_number, :status)
    end
end
