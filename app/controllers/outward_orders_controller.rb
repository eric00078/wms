class OutwardOrdersController < ApplicationController
  before_action :set_outward_order, only: %i[ show edit update destroy ]

  # GET /outward_orders or /outward_orders.json
  def index
    @outward_orders = OutwardOrder.all
  end

  # GET /outward_orders/1 or /outward_orders/1.json
  def show
  end

  # GET /outward_orders/new
  def new
    @outward_order = OutwardOrder.new
  end

  # GET /outward_orders/1/edit
  def edit
  end

  # POST /outward_orders or /outward_orders.json
  def create
    @outward_order = OutwardOrder.new(outward_order_params)

    respond_to do |format|
      if @outward_order.save
        format.html { redirect_to outward_order_url(@outward_order), notice: "Outward order was successfully created." }
        format.json { render :show, status: :created, location: @outward_order }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @outward_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /outward_orders/1 or /outward_orders/1.json
  def update
    respond_to do |format|
      if @outward_order.update(outward_order_params)
        format.html { redirect_to outward_order_url(@outward_order), notice: "Outward order was successfully updated." }
        format.json { render :show, status: :ok, location: @outward_order }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @outward_order.errors, status: :unprocessable_entity }
      end
    end
  end

  def orders_with_status
    render json: OutwardOrder.find_orders_by_status(params[:status])
  end

  # DELETE /outward_orders/1 or /outward_orders/1.json
  def destroy
    @outward_order.destroy

    respond_to do |format|
      format.html { redirect_to outward_orders_url, notice: "Outward order was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_outward_order
      @outward_order = OutwardOrder.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def outward_order_params
      params.require(:outward_order).permit(:order_number, :status)
    end
end
