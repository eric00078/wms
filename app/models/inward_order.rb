class InwardOrder < ApplicationRecord
  has_many :inward_order_details, primary_key: :order_number, foreign_key: :order_number
  validates_presence_of :order_number, :status

  def self.find_orders_by_status(status)
    Inward.where(status: status)
  end
end
