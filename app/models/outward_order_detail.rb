class OutwardOrderDetail < ApplicationRecord
  belongs_to :outward_order, foreign_key: :order_number
  validates_presence_of :order_number, :item_number, :amount, :status
end
