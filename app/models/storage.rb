class Storage < ApplicationRecord
  has_one :stock
  validates_presence_of :storage_id
  validates :in_use, inclusion: [true, false]

  def self.empty_storage
    Storage.where(in_use: 0).take
  end

  def self.empty_storages
    Storage.where(in_use: 0)
  end
end
