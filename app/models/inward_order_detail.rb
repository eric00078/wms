class InwardOrderDetail < ApplicationRecord
  belongs_to :inward_order, foreign_key: :order_number
  validates_presence_of :order_number, :item_number, :amount, :status
end
