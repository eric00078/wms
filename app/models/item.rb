class Item < ApplicationRecord
  has_many :stocks
  validates_presence_of :item_number, :amount
end
