class Stock < ApplicationRecord
  belongs_to :item, foreign_key: :item_number
  belongs_to :storage
  validates_presence_of :storage_id, :item_number, :amount

  def self.find_stocks_by_item(item_number)
    Stock.where(item_number: item_number).order(created_at: :asc)
  end
end
