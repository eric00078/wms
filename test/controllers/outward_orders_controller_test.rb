require "test_helper"

class OutwardOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @outward_order = outward_orders(:one)
  end

  test "should get index" do
    get outward_orders_url
    assert_response :success
  end

  test "should get new" do
    get new_outward_order_url
    assert_response :success
  end

  test "should create outward_order" do
    assert_difference("OutwardOrder.count") do
      post outward_orders_url, params: { outward_order: { order_number: "MyString3", status: "MyString" } }
    end

    assert_redirected_to outward_order_url(OutwardOrder.last)
  end

  test "should show outward_order" do
    get outward_order_url(@outward_order)
    assert_response :success
  end

  test "should get edit" do
    get edit_outward_order_url(@outward_order)
    assert_response :success
  end

  test "should update outward_order" do
    patch outward_order_url(@outward_order), params: { outward_order: { order_number: @outward_order.order_number, status: @outward_order.status } }
    assert_redirected_to outward_order_url(@outward_order)
  end

  test "should destroy outward_order" do
    assert_difference("OutwardOrder.count", -1) do
      delete outward_order_url(@outward_order)
    end

    assert_redirected_to outward_orders_url
  end
end
