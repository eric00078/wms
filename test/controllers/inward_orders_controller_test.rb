require "test_helper"

class InwardOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inward_order = inward_orders(:one)
  end

  test "should get index" do
    get inward_orders_url
    assert_response :success
  end

  test "should get new" do
    get new_inward_order_url
    assert_response :success
  end

  test "should create inward_order" do
    assert_difference("InwardOrder.count") do
      post inward_orders_url, params: { inward_order: { order_number: "MyString3", status: "MyString" } }
    end

    assert_redirected_to inward_order_url(InwardOrder.last)
  end

  test "should show inward_order" do
    get inward_order_url(@inward_order)
    assert_response :success
  end

  test "should get edit" do
    get edit_inward_order_url(@inward_order)
    assert_response :success
  end

  test "should update inward_order" do
    patch inward_order_url(@inward_order), params: { inward_order: { order_number: @inward_order.order_number, status: @inward_order.status } }
    assert_redirected_to inward_order_url(@inward_order)
  end

  test "should destroy inward_order" do
    assert_difference("InwardOrder.count", -1) do
      delete inward_order_url(@inward_order)
    end

    assert_redirected_to inward_orders_url
  end
end
