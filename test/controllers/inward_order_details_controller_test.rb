require "test_helper"

class InwardOrderDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inward_order_detail = inward_order_details(:one)
  end

  test "should get index" do
    get inward_order_details_url
    assert_response :success
  end

  test "should get new" do
    get new_inward_order_detail_url
    assert_response :success
  end

  test "should create inward_order_detail" do
    @inward_order = inward_orders(:one)
    assert_difference("InwardOrderDetail.count") do
      post inward_order_details_url, params: { inward_order_detail: { status: @inward_order_detail.status, amount: @inward_order_detail.amount, item_number: @inward_order_detail.item_number, order_number: @inward_order.order_number, serial_number: @inward_order_detail.serial_number } }
    end

    assert_redirected_to inward_order_detail_url(InwardOrderDetail.last)
  end

  test "should show inward_order_detail" do
    get inward_order_detail_url(@inward_order_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_inward_order_detail_url(@inward_order_detail)
    assert_response :success
  end

  test "should update inward_order_detail" do
    patch inward_order_detail_url(@inward_order_detail), params: { inward_order_detail: { amount: @inward_order_detail.amount, item_number: @inward_order_detail.item_number, order_number: @inward_order_detail.order_number, serial_number: @inward_order_detail.serial_number } }
    assert_redirected_to inward_order_detail_url(@inward_order_detail)
  end

  test "should destroy inward_order_detail" do
    assert_difference("InwardOrderDetail.count", -1) do
      delete inward_order_detail_url(@inward_order_detail)
    end

    assert_redirected_to inward_order_details_url
  end
end
