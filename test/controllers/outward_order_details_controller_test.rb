require "test_helper"

class OutwardOrderDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @outward_order_detail = outward_order_details(:one)
  end

  test "should get index" do
    get outward_order_details_url
    assert_response :success
  end

  test "should get new" do
    get new_outward_order_detail_url
    assert_response :success
  end

  test "should create outward_order_detail" do
    @outward_order = outward_orders(:one)
    assert_difference("OutwardOrderDetail.count") do
      post outward_order_details_url, params: { outward_order_detail: { status: @outward_order_detail.status, amount: @outward_order_detail.amount, item_number: @outward_order_detail.item_number, order_number: @outward_order.order_number, serial_number: @outward_order_detail.serial_number } }
    end

    assert_redirected_to outward_order_detail_url(OutwardOrderDetail.last)
  end

  test "should show outward_order_detail" do
    get outward_order_detail_url(@outward_order_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_outward_order_detail_url(@outward_order_detail)
    assert_response :success
  end

  test "should update outward_order_detail" do
    patch outward_order_detail_url(@outward_order_detail), params: { outward_order_detail: { amount: @outward_order_detail.amount, item_number: @outward_order_detail.item_number, order_number: @outward_order_detail.order_number, serial_number: @outward_order_detail.serial_number } }
    assert_redirected_to outward_order_detail_url(@outward_order_detail)
  end

  test "should destroy outward_order_detail" do
    assert_difference("OutwardOrderDetail.count", -1) do
      delete outward_order_detail_url(@outward_order_detail)
    end

    assert_redirected_to outward_order_details_url
  end
end
