require "application_system_test_case"

class InwardOrdersTest < ApplicationSystemTestCase
  setup do
    @inward_order = inward_orders(:one)
  end

  test "visiting the index" do
    visit inward_orders_url
    assert_selector "h1", text: "Inward orders"
  end

  test "should create inward order" do
    visit inward_orders_url
    click_on "New inward order"

    fill_in "Order number", with: @inward_order.order_number
    fill_in "Status", with: @inward_order.status
    click_on "Create Inward order"

    assert_text "Inward order was successfully created"
    click_on "Back"
  end

  test "should update Inward order" do
    visit inward_order_url(@inward_order)
    click_on "Edit this inward order", match: :first

    fill_in "Order number", with: @inward_order.order_number
    fill_in "Status", with: @inward_order.status
    click_on "Update Inward order"

    assert_text "Inward order was successfully updated"
    click_on "Back"
  end

  test "should destroy Inward order" do
    visit inward_order_url(@inward_order)
    click_on "Destroy this inward order", match: :first

    assert_text "Inward order was successfully destroyed"
  end
end
