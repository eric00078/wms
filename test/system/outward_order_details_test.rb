require "application_system_test_case"

class OutwardOrderDetailsTest < ApplicationSystemTestCase
  setup do
    @outward_order_detail = outward_order_details(:one)
  end

  test "visiting the index" do
    visit outward_order_details_url
    assert_selector "h1", text: "Outward order details"
  end

  test "should create outward order detail" do
    visit outward_order_details_url
    click_on "New outward order detail"

    fill_in "Amount", with: @outward_order_detail.amount
    fill_in "Item number", with: @outward_order_detail.item_number
    fill_in "Order number", with: @outward_order_detail.order_number
    fill_in "Serial number", with: @outward_order_detail.serial_number
    click_on "Create Outward order detail"

    assert_text "Outward order detail was successfully created"
    click_on "Back"
  end

  test "should update Outward order detail" do
    visit outward_order_detail_url(@outward_order_detail)
    click_on "Edit this outward order detail", match: :first

    fill_in "Amount", with: @outward_order_detail.amount
    fill_in "Item number", with: @outward_order_detail.item_number
    fill_in "Order number", with: @outward_order_detail.order_number
    fill_in "Serial number", with: @outward_order_detail.serial_number
    click_on "Update Outward order detail"

    assert_text "Outward order detail was successfully updated"
    click_on "Back"
  end

  test "should destroy Outward order detail" do
    visit outward_order_detail_url(@outward_order_detail)
    click_on "Destroy this outward order detail", match: :first

    assert_text "Outward order detail was successfully destroyed"
  end
end
