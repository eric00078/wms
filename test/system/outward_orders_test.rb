require "application_system_test_case"

class OutwardOrdersTest < ApplicationSystemTestCase
  setup do
    @outward_order = outward_orders(:one)
  end

  test "visiting the index" do
    visit outward_orders_url
    assert_selector "h1", text: "Outward orders"
  end

  test "should create outward order" do
    visit outward_orders_url
    click_on "New outward order"

    fill_in "Order number", with: @outward_order.order_number
    fill_in "Status", with: @outward_order.status
    click_on "Create Outward order"

    assert_text "Outward order was successfully created"
    click_on "Back"
  end

  test "should update Outward order" do
    visit outward_order_url(@outward_order)
    click_on "Edit this outward order", match: :first

    fill_in "Order number", with: @outward_order.order_number
    fill_in "Status", with: @outward_order.status
    click_on "Update Outward order"

    assert_text "Outward order was successfully updated"
    click_on "Back"
  end

  test "should destroy Outward order" do
    visit outward_order_url(@outward_order)
    click_on "Destroy this outward order", match: :first

    assert_text "Outward order was successfully destroyed"
  end
end
