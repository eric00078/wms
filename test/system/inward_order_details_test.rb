require "application_system_test_case"

class InwardOrderDetailsTest < ApplicationSystemTestCase
  setup do
    @inward_order_detail = inward_order_details(:one)
  end

  test "visiting the index" do
    visit inward_order_details_url
    assert_selector "h1", text: "Inward order details"
  end

  test "should create inward order detail" do
    visit inward_order_details_url
    click_on "New inward order detail"

    fill_in "Amount", with: @inward_order_detail.amount
    fill_in "Item number", with: @inward_order_detail.item_number
    fill_in "Order number", with: @inward_order_detail.order_number
    fill_in "Serial number", with: @inward_order_detail.serial_number
    click_on "Create Inward order detail"

    assert_text "Inward order detail was successfully created"
    click_on "Back"
  end

  test "should update Inward order detail" do
    visit inward_order_detail_url(@inward_order_detail)
    click_on "Edit this inward order detail", match: :first

    fill_in "Amount", with: @inward_order_detail.amount
    fill_in "Item number", with: @inward_order_detail.item_number
    fill_in "Order number", with: @inward_order_detail.order_number
    fill_in "Serial number", with: @inward_order_detail.serial_number
    click_on "Update Inward order detail"

    assert_text "Inward order detail was successfully updated"
    click_on "Back"
  end

  test "should destroy Inward order detail" do
    visit inward_order_detail_url(@inward_order_detail)
    click_on "Destroy this inward order detail", match: :first

    assert_text "Inward order detail was successfully destroyed"
  end
end
